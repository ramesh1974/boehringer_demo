import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ApiServiceService } from '../api-service.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.css']
})
export class AllComponent implements OnInit {

  lst = []; all = []; filter = ''; filterLst = [];

  constructor(private apiServ: ApiServiceService,private router :Router) { }

  ngOnInit(): void {
    this.LoadData();
    // Promise.all([this.LoadData(),this.LoadData()]).then(()=>{
    //   console.log();
    // })
  }
  
  LoadData() {
    this.apiServ.getAll().subscribe(data =>{
      this.lst = this.all = data;
       data.forEach(e => {
         this.filterLst.push(e.userId);
      });
      let nw = new Set(this.filterLst);
      this.filterLst = [...nw];
      console.log(this.lst)
    }, 
    error => {console.log(error)});
  }

onChange(val) {
  this.lst = this.all;
  if(val != '')
  this.lst = this.lst.filter(f=>f.userId == val);
}

  deleteUser(id: number) {
    console.log(id)
    this.apiServ.delete(id)
        .subscribe(data =>{
          console.log(data)
        }, 
        error => {console.log(error)});
        this.router.navigate(['/home/all']);
}

navigateToPostDetails(id: number): void {
  console.log('id'+id)
  this.router.navigate(['/home/posts/'+id]);
}

navigateToCreateDetails(): void {
  this.router.navigate(['/home/create-post']);
}

navigateToUpdateDetails(data : any): void {
  localStorage.setItem('update_details', JSON.stringify(data));
  console.log(localStorage.getItem('update_details'));
  this.router.navigate(['/home/create-post']);
}

}
