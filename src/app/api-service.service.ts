import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  constructor(private http: HttpClient) { }

  // save(apiMethod: string, data: any): Observable<any> { return this.http.post(`${this.registrationAPI}${apiMethod}`, data); }
  getAll(): Observable<any> { return this.http.get(`https://jsonplaceholder.typicode.com/posts`); }
  getbyid(id:Number): Observable<any> { return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`); }
  delete(id: Number) {return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);}
  createPost(data: any): Observable<any> { return this.http.post<any>(`https://jsonplaceholder.typicode.com/posts`, data);}
  updatePost(data: any,id : Number): Observable<any> { return this.http.patch<any>(`https://jsonplaceholder.typicode.com/posts/${id}`, data);}
}
