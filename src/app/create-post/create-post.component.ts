import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators,ReactiveFormsModule  } from '@angular/forms';
import { ApiServiceService } from '../api-service.service';
import { first } from 'rxjs/operators';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

  registerForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,private apiServ: ApiServiceService
    ) {
       
    }
    dataupdate : any;
    ngOnInit() {

      this.dataupdate = JSON.parse(localStorage.getItem('update_details'));   
      if(this.dataupdate != null)
      {
       
        localStorage.removeItem('update_details');
        console.log('data :',this.dataupdate.id)
        this.registerForm = this.formBuilder.group({
          id: [this.dataupdate.id, Validators.required],
          userid: [this.dataupdate.userId, Validators.required],
          title: [this.dataupdate.title, Validators.required],
          body: [this.dataupdate.body, Validators.required]
      });
      }
      else{
        this.registerForm = this.formBuilder.group({
            userid: ['', Validators.required],
            title: ['', Validators.required],
            body: ['', Validators.required]
        });
      }
    }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }
    data : {};
    onSubmit() {
        this.submitted = true;


        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        if(this.registerForm.value.id >= 0) 
        {
          this.loading = true;
          this.apiServ.updatePost(this.registerForm.value,this.registerForm.value.id).subscribe(res =>{
            this.data = res;
            console.log(this.data)
            this.router.navigate(['/home/all']);
          }, 
          error => {console.log(error)});

        }
        else {
        console.log("create value :"+this.registerForm.value.id);
        this.loading = true;
        this.apiServ.createPost(this.registerForm.value).subscribe(res =>{
          this.data = res;
          console.log(this.data)
          this.router.navigate(['/home/all']);
        }, 
        error => {console.log(error)});
      }


    }
}

