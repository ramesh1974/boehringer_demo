import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  data : any = {};
  public loading = false;
  constructor(private apiServ: ApiServiceService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    let id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.LoadData(id);
  }

  async LoadData(id) {
    this.loading = true;
    this.apiServ.getbyid(id).subscribe(res =>{
      this.data = res;
      console.log(this.data)
    }, 
    error => {console.log(error)});
  }

}
