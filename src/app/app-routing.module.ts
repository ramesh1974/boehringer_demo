import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { PostsComponent } from './posts/posts.component';
import { AllComponent } from './all/all.component';
import { CreatePostComponent } from './create-post/create-post.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, children: [
    {path:'all',component:AllComponent},
    {path:'posts/:id',component:PostsComponent},
    {path:'create-post',component:CreatePostComponent},
  ]},
  {path:'',redirectTo :'home',pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
