import { Component, OnInit } from '@angular/core';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  lst = [];

  constructor(private apiServ: ApiServiceService) { }

  ngOnInit(): void {
    this.LoadData();
  }
  
  LoadData() {
    this.apiServ.getAll().subscribe(data =>{
      this.lst = data;
      console.log(this.lst)
    }, 
    error => {console.log(error)});
  }

}
